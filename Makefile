#!make
include .env
export $(shell sed 's/=.*//' .env)

docker-env: db-config clone-backend generate-jwt hosts generate-ssl nginx-config symfony-config up

clone-backend:
	@echo "\n\033[1;mCloning Backend repository (${BACKEND_BRANCH_NAME} branch) \033[0m"
	@bash -c "if [ -d ./src/backend ]; then cd ./src/backend && git pull origin ${BACKEND_BRANCH_NAME}; else git clone -b ${BACKEND_BRANCH_NAME} ${BACKEND_GIT_URL} ./src/backend; fi"
clone-frontend:
	@echo "\n\033[1;mCloning Frontend repository (${FRONTEND_BRANCH_NAME} branch) \033[0m"
	@bash -c "if [ -d ./src/frontend ]; then cd ./src/frontend && git pull origin ${FRONTEND_BRANCH_NAME}; else git clone -b ${FRONTEND_BRANCH_NAME} ${FRONTEND_GIT_URL} ./src/frontend; fi"

rebuild: stop
	@echo "\n\033[1;m Rebuilding containers... \033[0m"
	@docker-compose build --no-cache
frontend-build:
	@. ./scripts/sh-config-frontend.sh
	@docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-frontend.yml up
	@docker-compose -p ${COMPOSE_PROJECT_NAME} -f ./docker-compose-frontend.yml rm -sfv
up:
	@echo "\n\033[1;m Spinning up containers for ${ENVIRONMENT} environment... \033[0m"
	@docker-compose -p ${COMPOSE_PROJECT_NAME} up -d
	@$(MAKE) --no-print-directory status
stop:
	@echo "\n\033[1;m Halting containers... \033[0m"
	@docker-compose stop
	@$(MAKE) --no-print-directory status
restart:
	@$(MAKE) --no-print-directory stop
	@$(MAKE) --no-print-directory up
status:
	@echo "\n\033[1;m Containers statuses \033[0m"
	@docker-compose ps
	@docker-compose -f ./docker-compose-mock.yml ps

generate-jwt:
	@echo "\n\033[1;mGenerating jwt keys\033[0m"
	@bash -c "if [ -a ./app/jwt/private_key.pem ]; then echo 'JWT private key already exist'; else openssl genrsa -aes256 -out ./app/jwt/private_key.pem -passout pass:${JWT_PASSPHRASE} 2048 2> /dev/null && echo 'JWT private key was generated'; fi"
	@bash -c "if [ -a ./app/jwt/public_key.pem ]; then echo 'JWT public key already exist'; else openssl rsa -in ./app/jwt/private_key.pem -passin pass:${JWT_PASSPHRASE} -pubout -out ./app/jwt/public_key.pem 2> /dev/null && echo 'JWT public key was generated'; fi"

composer-install:
	@docker-compose exec app bash -c "cd /var/www/${APP_NAME}/ && php -d memory_limit=-1 /usr/local/bin/composer install"

cache-clear:
	@docker-compose exec app bash -c "cd /var/www/${APP_NAME} && php bin/console cache:clear --no-warmup"

nginx-config:
	@. ./scripts/sh-config-nginx.sh

symfony-config:
	@. ./scripts/sh-config-symfony.sh

db-config:
	@. ./scripts/sh-mysql-grants.sh
db-create:
	@echo "\n\033[1;m Creating new empty database \033[0m"
	@docker-compose exec db bash -c "mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} -e 'CREATE SCHEMA IF NOT EXISTS \`${MYSQL_DATABASE}\`;'"
db-drop:
	@echo "\n\033[1;m Removing old database \033[0m"
	@docker-compose exec db bash -c "mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} -e 'DROP SCHEMA IF EXISTS \`${MYSQL_DATABASE}\`;'"
db-schema-update:
	@docker-compose exec app bash -c "cd /var/www/${APP_NAME}/ && php bin/console doctrine:schema:update --force --dump-sql"
db-dump:
	@docker-compose exec db bash -c "mysqldump -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} ${table} > ./docker-entrypoint-initdb.d/${sql}"
db-dump-apply:
	@echo "\n\033[1;m Applying dump from mysql/dump/$(sql) \033[0m"
	@docker-compose exec db bash -c "mysql -u${MYSQL_USER} -p${MYSQL_PASSWORD} ${MYSQL_DATABASE} < ./docker-entrypoint-initdb.d/$(sql)"
db-dump-list:
	@echo "Listing dumps"
	@bash -c "ls -la mysql/dump/*.sql"

generate-ssl:
	@echo "Generating SSL certificate"
	@bash ./scripts/sh-generate-ssl.sh
hosts:
	@echo "\n\033[1;m Adding record in to your local hosts file.\033[0m"
	@echo "\n\033[1;m Please use your local sudo password.\033[0m"
	@echo "127.0.0.1 localhost ${APP_NAME}.${ENVIRONMENT}.sg.umbrella-web.com" | sudo tee -a /etc/hosts

console-app:
	@docker-compose exec app bash
console-db:
	@docker-compose exec db bash
console-nginx:
	@docker-compose exec web-srv bash

logs-nginx:
	@docker-compose logs --tail=100 -f web-srv
logs-app:
	@docker-compose logs --tail=100 -f app
logs-db:
	@docker-compose logs --tail=100 -f db
