cat <<EOF > ./nginx/configs/conf.d/${APP_NAME}-${ENVIRONMENT}.conf
upstream php {
    server app:9000;
}

server {
    server_name www.${BACKEND_SERVER_NAME};
    return 301 \$scheme://${BACKEND_SERVER_NAME}\$request_uri;
}

server {
    server_name ${BACKEND_SERVER_NAME};

    listen 80;
    listen 443 ssl;

    ssl_certificate /etc/nginx/ssl/${APP_NAME}.cert;
    ssl_certificate_key /etc/nginx/ssl/${APP_NAME}.key;

    root /var/www/${APP_NAME}/public;

    # CORS
    error_page 401 = @corsopts;
    error_page 413 = @corsopts413;

    location / {
        try_files \$uri /index.php\$is_args\$args;
        proxy_set_header Host \$host;
    }

    location ~ ^/index\.php(/|$) {
        fastcgi_pass php;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME \$realpath_root\$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT \$realpath_root;
        internal;

		if (\$request_method = 'OPTIONS') {

			add_header 'Access-Control-Allow-Origin' \$http_Origin;
			add_header 'Access-Control-Allow-Credentials' 'true';
			add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, DELETE';
			add_header 'Access-Control-Allow-Headers' 'XAuthToken,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,X-RequestTimeZone,Authorization';

			add_header 'Access-Control-Max-Age' 1728000;
			add_header 'Content-Type' 'text/plain charset=UTF-8';
			add_header 'Content-Length' 0;
			return 204;
		}

		add_header 'Access-Control-Allow-Origin' \$http_Origin always;
        add_header 'Access-Control-Allow-Credentials' 'true' always;
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, DELETE' always;
        add_header 'Access-Control-Allow-Headers' 'XAuthToken,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization,X-RequestTimeZone,Authorization' always;
    }

    location @corsopts {

        if (\$request_method = 'OPTIONS') {

            add_header 'Access-Control-Allow-Origin' \$http_Origin;
            add_header 'Access-Control-Allow-Credentials' 'true';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, DELETE';
            add_header 'Access-Control-Allow-Headers' 'XAuthToken,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization,X-RequestTimeZone,Authorization';

            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Content-Type' 'text/plain charset=UTF-8';
            add_header 'Content-Length' 0;
            return 204;
        }

        add_header 'Access-Control-Allow-Origin' \$http_Origin always;
        add_header 'Access-Control-Allow-Credentials' 'true' always;
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, DELETE' always;
        add_header 'Access-Control-Allow-Headers' 'XAuthToken,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization,X-RequestTimeZone,Authorization' always;

        return 401;
    }

    location @corsopts413 {

        if (\$request_method = 'OPTIONS') {

            add_header 'Access-Control-Allow-Origin' \$http_Origin;
            add_header 'Access-Control-Allow-Credentials' 'true';
            add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, DELETE';
            add_header 'Access-Control-Allow-Headers' 'XAuthToken,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization,X-RequestTimeZone,Authorization';

            add_header 'Access-Control-Max-Age' 1728000;
            add_header 'Content-Type' 'text/plain charset=UTF-8';
            add_header 'Content-Length' 0;
            return 204;
        }

        add_header 'Access-Control-Allow-Origin' \$http_Origin always;
        add_header 'Access-Control-Allow-Credentials' 'true' always;
        add_header 'Access-Control-Allow-Methods' 'GET, POST, OPTIONS, PUT, DELETE' always;
        add_header 'Access-Control-Allow-Headers' 'XAuthToken,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization,X-RequestTimeZone,Authorization' always;

        return 413;
    }

    location ~ \.php$ {
        return 404;
    }
}

server {
    server_name www.${FRONTEND_SERVER_NAME};
    return 301 \$scheme://${FRONTEND_SERVER_NAME}\$request_uri;
}

server {
    server_name ${FRONTEND_SERVER_NAME};

    listen 80;
    listen 443 ssl;

    ssl_certificate /etc/nginx/ssl/${APP_NAME}.cert;
    ssl_certificate_key /etc/nginx/ssl/${APP_NAME}.key;

    location / {
        root /var/www/${APP_NAME}-frontend/build;
        try_files \$uri\$args \$uri\$args/ /index.html;
    }
}
EOF
