#!/bin/bash
{ \
	echo "CREATE SCHEMA IF NOT EXISTS ${MYSQL_DATABASE};"; \
	echo "GRANT ALL PRIVILEGES ON *.* TO '${MYSQL_USER}'@'%' IDENTIFIED BY '${MYSQL_PASSWORD}' WITH GRANT OPTION;"; \
    echo "FLUSH PRIVILEGES;"; \
}| tee mysql/dump/grants.sql