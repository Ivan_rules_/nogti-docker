#!/bin/bash

if [ -f ./src/frontend/.env ]; then
   rm ./src/frontend/.env
fi

cat <<EOF > ./src/frontend/.env
###########################################################################
# Application configuration

# Version
REACT_APP_VERSION=\$npm_package_version

# Router prefix
REACT_APP_ROUTER_PREFIX=

# API url path
REACT_APP_API_URL=${SERVER_PROTOCOL}://${BACKEND_SERVER_NAME}
# API request timeout
REACT_APP_API_REQUEST_TIMEOUT=

# Required to build absolute import paths in typescript
NODE_PATH=./
EOF
